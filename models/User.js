const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const {nanoid} = require("nanoid");

const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    displayName: {
      type: String,
      required: true,
    },
    imageLink: {
      type: String,
    },
    email: {
        type: String,
        required: true,
    },
    sharedId: {
        type: Array,
    },
    shared: {
        type: Array,
    },
    role: {
        type: String,
        required: true,
        default: "user",
        enum: ["user", "admin"],
    },
    facebookId: String,
});

UserSchema.pre("save", async function(next) {
    if (!this.isModified("password")) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

UserSchema.set("toJSON", {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = function(password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.shareWith = function(shared) {
    this.shared = shared;
};

UserSchema.methods.shareInfo = function(id) {
    this.sharedId = id;
};

UserSchema.methods.generateToken = function() {
    this.token = nanoid();
};

const User = mongoose.model("User", UserSchema);

module.exports = User;