const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const EventSchema = new Schema ({
    date: {
        type: String,
        required: true,
    },
    isoDate: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
});

EventSchema.methods.getAuthor = function (author) {
    this.author = author;
};

const Event = mongoose.model("Event", EventSchema);

module.exports = Event;