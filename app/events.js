const router = require("express").Router();
const Event = require("../models/Event");
const auth = require("../middleware/auth")

router.get("/:id", async (req,res) => {
    const result = await Event.find({author: req.params.id}).sort({isoDate: 1});
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});

router.post("/", auth, async (req,res) => {
    try {
        if (req.body.date === "") {
            return res.status(400).send({error: "Please, add date."});
        }
        if (req.body.description === "") {
            return res.status(400).send({error: "Please, add description."});
        }
        const event = new Event(req.body);
        event.getAuthor(req.author._id);
        await event.save();
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete("/:id", auth, async (req,res) => {
    try {
        await Event.findByIdAndDelete(req.params.id);
        res.send({message: "Deleted"});
    } catch (e) {
        return res.status(400).send(e);
    }
});

module.exports = router;