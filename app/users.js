const router = require("express").Router();
const User = require("../models/User");
const config = require("../config");
const auth = require("../middleware/auth");
const axios = require("axios");
const {nanoid} = require("nanoid");

router.get ("/shareWith", async (req, res) => {
    res.status(400).send({error: "Please, add email."});
});

router.get("/shareWith/:email", async (req, res) => {
    try {
        const user = await User.findOne({email: req.params.email});
        if (!user) {
            return res.status(400).send({error: "User is not found."});
        }
        res.send(user);
    } catch (e) {
        res.sendStatus(400).send(e);
    }
});

router.delete("/shareWith/delete/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.author._id);
        user.shared.map(async (key) => {
            if (key.id == req.params.id) {
                await user.shared.splice(key, 1);
            }
        });

        user.sharedId.map(async (key) => {
            if (key === req.params.id) {
                console.log("fucked")
                await user.sharedId.splice(key, 1);
            }
        })
        await user.save();
    } catch (e) {
        res.sendStatus(400).send(e);
    }
});

router.post("/shareWith/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.author._id);
        const share = user.shared;
        const sharedId = user.sharedId;
        const sharedUser = await User.findById(req.params.id);
        const users = {
            id: sharedUser._id,
            name: sharedUser.displayName,
            image: sharedUser.imageLink,
        }
        sharedId.push(req.params.id);
        share.push(users);
        user.shareWith(share);
        await user.save();
    } catch (e) {
        res.sendStatus(400).send(e);
    }
});

router.get("/shared/:id", async (req, res) => {
    const result = await User.find({sharedId: req.params.id});
    const shared = [];
    Object.keys(result).forEach(key => {
        const events = {
            name: result[key].displayName,
            id: result[key]._id,
            image: result[key].imageLink,
        }
        shared.push(events);
    })
    res.send(shared);
});

router.post("/", async (req, res) => {
    try {
        const checkUser = await User.findOne({username: req.body.username});
        if (checkUser) {
            return res.status(400).send({error: "This username is already taken"});
        }
        if (req.body.username === "") {
            return res.status(400).send({error: "Please, add Username."});
        }
        if (req.body.displayName === "") {
            return res.status(400).send({error: "Please, add Display Name."});
        }
        if (req.body.password === "") {
            return res.status(400).send({error: "Please, create Password."});
        }
        if (req.body.email === "") {
            return res.status(400).send({error: "Please, add your email"});
        }
        const checkEmail = await User.findOne({email: req.body.email});
        if (checkEmail) {
            return res.status(400).send({error: "This email is already using"});
        }
        const user = new User({
            username: req.body.username,
            password: req.body.password,
            displayName: req.body.displayName,
            imageLink: req.body.imageLink,
            email: req.body.email,
        });
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post("/facebookLogin", async (req,res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.fb.appId + "|" + config.fb.appSecret;
    const debugToken = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
    try {
        const response = await axios.get(debugToken);
        if (response.data.data.error) {
            return res.status(401).send({message: "Facebook token incorrect"});
        }
        if (response.data.data.user_id !== req.body.id) {
            return res.status(401).send({message: "Wrong user ID"});
        }
        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.name,
                password: nanoid(),
                facebookId: req.body.id,
                imageLink: req.body.picture.data.url,
                displayName: req.body.name,
                email: req.body.email,
            });
        }
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(401).send({message: "Facebook token incorrect"});
    }
});

router.post("/sessions", async (req, res) => {
    const user = await User.findOne({username: req.body.username});
    if (!user) {
        return res.status(400).send({error: "Username not found"});
    }
    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) {
        return res.status(400).send({error: "Password is wrong"});
    }
    user.generateToken();
    await user.save();
    res.send(user);
});

router.delete("/sessions", async (req, res) => {
    const token = req.get("Authorization");
    const success = {message: "Success"};

    if (!token) return res.send(success);
    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    user.save();

    return res.send(success);
});

module.exports = router;